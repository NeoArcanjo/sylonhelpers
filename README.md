config :httpoison,
  timeout: 300_000,
  recv_timeout: 300_000,
  max_attempts: 5,
  wait: 15_000,
  include_404s: false,
  retry_unknown_errors: false


  import AlertrackHelpers.Request, only: [autoretry: 2]

  # configurações pré-definidas de timeout para as requisições
  @timeout Application.get_env(:httpoison, :timeout)
  @recv_timeout Application.get_env(:httpoison, :recv_timeout)


  HTTPoison.get(@url <> cpf,
      hackney: [:insecure, timeout: @timeout, recv_timeout: @recv_timeout]
    )
    |> autoretry(retry_unknown_errors: true)