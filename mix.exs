defmodule AlertrackHelpers.MixProject do
  use Mix.Project

  def project do
    [
      app: :alertrack_helpers,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {AlertrackHelpers.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:httpoison, "~> 1.6"},
      {:nimble_csv, "~> 0.6"},
      {:opentelemetry_api, "~> 0.3.2"},
      {:opentelemetry_zipkin, "~> 0.2.0"},
      {:opentelemetry, "~> 0.4.0"},
      {:credo, "> 0.0.0", only: [:dev, :test]},
      {:elastix, ">= 0.0.0", optional: true},
      {:timex, "~> 3.6", optional: true}
    ]
  end
end

# curl -XGET "http://localhost:9200/bolsa_familia_log_*/_search" -H 'Content-Type: application/json' -d'{  "from": 0,  "size": 100,   "query": {    "bool": {      "must": [        {          "exists": {            "field": "data.beneficiario"          }        },        {          "bool": {            "must_not": [              {                "exists": {                  "field": "persisted"                }              }            ]          }        }      ]    }  }}'
