defmodule AlertrackHelpers.FindRegisteredCpf do
  @timeout Application.get_env(:httpoison, :timeout)
  @recv_timeout Application.get_env(:httpoison, :recv_timeout)
  import AlertrackHelpers.Request, only: [autoretry: 2]
  require Logger

  # AlertrackHelpers.FindRegisteredCpf.main
  # HTTPoison.get("http://69.69.69.181:4000/api/get-cpfs?page=#20&page_size=10", [{"Content-Type", "application/json"}])

  # {:ok, %_{body: body}} = HTTPoison.get("http://69.69.69.181:4000/api/get-cpfs?page=#20&page_size=10", [{"Content-Type", "application/json"}])

  @false_positive_api [
    SpawnC.Detran.Detran_msController
    # SpawnC.Detran.Detran_toController, 
    # SpawnC.PagamentosEletronicos.Move_maisController, 
    # SpawnC.PagamentosEletronicos.PagBemController
  ]

  def main do
    page = 92672
    size = 20
    {:ok, %_{body: body}} = get_lista_cpf(page, size)

    body =
      body
      |> Jason.decode!()
      |> Map.get("data")

    Enum.map(body, fn x ->
      %{"cpf" => cpf, "name" => _nome} = x
      IO.inspect(cpf)
      test_api(cpf)
    end)
  end

  # OWASP ZAP
  def get_lista_cpf(page, size) do
    HTTPoison.get(
      "http://69.69.69.181:4000/api/get-cpfs?page=#{page}&page_size=#{size}",
      [{"Content-Type", "application/json"}],
      hackney: [:insecure, timeout: @timeout, recv_timeout: @recv_timeout]
    )
    |> autoretry(retry_unknown_errors: true)
  end

  def test_api(cpf) do
    IO.inspect(cpf)

    @false_positive_api
    |> IO.inspect()
    |> Enum.map(&(apply(&1, :test_response, [cpf]) |> intercept_response(&1, cpf)))
  end

  def intercept_response(true, _modulo, _cpf) do
    nil
  end

  def intercept_response(false, modulo, cpf) do
    Logger.debug(
      "{\"CPF\":\"#{cpf}\",\"API\":\"#{modulo}\",\"MSG\":\"Retorno desconhecido, possível CPF CADASTRADO!\"}"
    )
  end

  # AlertrackHelpers.FindRegisteredCpf.main
end
