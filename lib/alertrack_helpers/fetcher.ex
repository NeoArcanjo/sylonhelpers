defmodule AlertrackHelpers.Fetcher do
  @endpoint Application.get_env(:alertrack_helpers, :consulta_cpf_api)

  alias AlertrackHelpers.Pipeline
  use Pipeline, :help_telemetry
  use Pipeline, :help_requires

  @doc """
  Envia os 100 primeiros registros do Banco de dados
  """
  def list_pfs do
    OpenTelemetry.Tracer.with_span "Api_Fetcher_list_pfs:0" do
      span_ctx = span_ctx_encode()

      query("cpf", 100)
      |> query_exec(span_ctx)
    end
  end

  @doc """
  Envia os 100 primeiros registros do Banco de dados a partir de um cursor
  """
  def list_pfs(cursor) do
    OpenTelemetry.Tracer.with_span "Api.Fetcher.list_pfs:1" do
      span_ctx = span_ctx_encode()

      query("cpf", 100, cursor)
      |> query_exec(span_ctx)
    end
  end

  def get_data_from_elastic(fields \\ "cpf", opts) when is_list(opts) do
    OpenTelemetry.Tracer.with_span "[Api.Fetcher] get_cpf_from_elastic/2" do
      span_ctx = span_ctx_encode()
      limit = opts[:limit] || 1
      search_after = opts[:cursor] || opts[:search_after]
      filter = opts[:filter]

      if(filter,
        do: query_from_elastic(fields, limit, filter, search_after),
        else: query_from_elastic(fields, limit, search_after)
      )
      |> query_exec(span_ctx)
    end
  end

  def get_pfs(fields \\ "cpf", opts) when is_list(opts) do
    OpenTelemetry.Tracer.with_span "Api.Fetcher.get_pfs:2" do
      limit = opts[:limit] || 1
      cursor = opts[:cursor]
      filter = opts[:filter]

      span_ctx = span_ctx_encode()

      if filter do
        filtered_query(fields, limit, filter, cursor)
      else
        query(fields, limit, cursor)
      end
      |> query_exec(span_ctx)
    end
  end

  # def get_pfs(fields \\ "cpf", limit \\ 100, cursor \\ nil) when is_integer(limit) and (is_bitstring(cursor) or is_nil(cursor)) do
  #   OpenTelemetry.Tracer.with_span "Api.Fetcher.get_pfs:3" do
  #     span_ctx = span_ctx_encode()

  #     query(fields, limit, cursor)
  #     |> query_exec(span_ctx)
  #   end
  # end

  # @doc """
  # Envia os campos selecionados, dos N primeiros registros do Banco de dados, segundo um filtro dado, a partir de um cursor opcional
  # """
  # def get_pfs(fields, limit, filter, cursor \\ nil) when is_map(filter)  do
  #   OpenTelemetry.Tracer.with_span "Api.Fetcher.get_pfs:4" do
  #     span_ctx = span_ctx_encode()

  #     filtered_query(fields, limit, filter, cursor)
  #     |> query_exec(span_ctx)
  #   end
  # end

  # def get_pfs(fields, limit, filter, cursor) do
  #   OpenTelemetry.Tracer.with_span "Api.Fetcher.get_pfs:4" do
  #     span_ctx = span_ctx_encode()

  #     filtered_query(fields, limit, filter, cursor)
  #     |> query_exec(span_ctx)
  #   end
  # end

  @doc """
  Seleciona os campos no individuo portador do CPF buscado
  """
  def get_pf(fields, cpf) do
    OpenTelemetry.Tracer.with_span "Api.Fetcher.get_pf:2" do
      span_ctx = span_ctx_encode()

      filtered_query(fields, 1, %{cpf: cpf}, nil)
      |> query_exec(span_ctx)
    end
  end

  @doc """
  Seleciona os campos de um CPF buscado, segundo filtros. Pode trazer CPF's não vinculados
  """
  def get_cpf_combination(fields, filter) do
    OpenTelemetry.Tracer.with_span "Api_Fetcher_get_cpf_combination:2" do
      span_ctx = span_ctx_encode()

      searched_cpfs(fields, filter)
      |> query_exec(span_ctx)
    end
  end

  # def get_cpf_combinations(fields, limit, filter, cursor \\ nil)

  # def get_cpf_combinations(fields, limit, filter, cursor) do
  #   OpenTelemetry.Tracer.with_span "Api_Fetcher_get_cpf_combination:2" do
  #     span_ctx = span_ctx_encode()

  #     searched_cpfs(fields, filter, limit, cursor)
  #     |> query_exec(span_ctx)
  #   end
  # end

  def get_cpf_combinations(fields, opts) do
    OpenTelemetry.Tracer.with_span "Api_Fetcher_get_cpf_combinations:2" do
      limit = opts[:limit] || 1
      cursor = opts[:cursor]
      filter = opts[:filter]

      span_ctx = span_ctx_encode()

      searched_cpfs(fields, filter, limit, cursor)
      |> query_exec(span_ctx)
    end
  end

  def get_cns(fields, opts) do
    OpenTelemetry.Tracer.with_span "Api.Fetcher.get_cns:2" do
      cursor = opts[:cursor]
      limit = opts[:limit]
      span_ctx = span_ctx_encode()

      if cursor do
        query_cns(fields, limit, cursor)
      else
        query_cns(fields, limit)
      end
      |> query_exec(span_ctx)
    end
  end

  def set_birthday(cpf, birthday) do
    OpenTelemetry.Tracer.with_span "Api_Fetcher_set_birthday:2" do
      span_ctx = span_ctx_encode()

      set_birthday_mutation(cpf, birthday)
      |> query_exec(span_ctx)
    end
  end

  def set_cns_status(cpf, cns_status, cns \\ nil) do
    OpenTelemetry.Tracer.with_span "Api.Fetcher.set_cns_status:3" do
      span_ctx = span_ctx_encode()

      set_cns_status_mutation(cpf, cns_status, cns)
      |> query_exec(span_ctx)
    end
  end

  def set_location(cpf, uf) do
    OpenTelemetry.Tracer.with_span "Api.Fetcher.set_location:2" do
      span_ctx = span_ctx_encode()

      set_location_mutation(cpf, uf)
      |> query_exec(span_ctx)
    end
  end

  def add_cpf_combination(cpf, name) do
    OpenTelemetry.Tracer.with_span "Api_Fetcher_add_cpf_combination:2" do
      span_ctx = span_ctx_encode()

      add_cpf_combination_mutation(cpf, name)
      |> query_exec(span_ctx)
    end
  end

  def update_cpf_combination(cpf, name) do
    OpenTelemetry.Tracer.with_span "Api_Fetcher_update_cpf_combination:2" do
      span_ctx = span_ctx_encode()

      update_cpf_combination_mutation(cpf, name)
      |> query_exec(span_ctx)
    end
  end

  def add_aux_emergencial(cpf, valor, nis, cidade_cod, mes, rpr_legal_cpf) do
    OpenTelemetry.Tracer.with_span "Api_Fetcher_add_aux_emergencial:4" do
      span_ctx = span_ctx_encode()

      add_aux_emergencial_mutation(cpf, valor, nis, cidade_cod, mes, rpr_legal_cpf)
      |> query_exec(span_ctx)
    end
  end

  defp update_cpf_combination_mutation(cpf, name) do
    ~s[
      mutation{
        addCpfCombination(cpf:\"#{cpf}\",input:{name:\"#{name}\"})
        {
          result{
            cpf
            name
          }
          message
          status
          }
        }
     ]
  end

  defp add_cpf_combination_mutation(cpf, nil) do
    ~s[
      mutation{
        addCpfCombination(input:{cpf:\"#{cpf}\"})
        {
          result{
            cpf
          }
          message
          status
          }
        }
     ]
  end

  defp add_cpf_combination_mutation(cpf, name) do
    ~s[
      mutation{
        addCpfCombination(input:{cpf:\"#{cpf}\",name:\"#{name}\"})
        {
          result{
            cpf
            name
          }
          message
          status
          }
        }
     ]
  end

  defp set_location_mutation(cpf, uf) do
    ~s[
      mutation{
        setCpfLocation(input:{cpf:\"#{cpf}\",uf:\"#{uf}\"})
        {
          status
          message
        }
      }
    ]
  end

  defp set_birthday_mutation(cpf, birthday) do
    ~s[
      mutation{
        setCpfBirthday(input:{cpf:\"#{cpf}\",birthday:\"#{birthday}\"})
        {
          status
          message
        }
      }
    ]
  end

  defp set_cns_status_mutation(cpf, cns_status, cns) when is_nil(cns) do
    ~s[
      mutation{
        setCnsStatus(input: {cpf:#{cpf}, has_cns:#{cns_status}}){
          status
          message
        }
      }
    ]
  end

  defp set_cns_status_mutation(cpf, cns_status, cns) do
    ~s[
      mutation{
        setCnsStatus(input: {cpf:\"#{cpf}\",has_cns:#{cns_status}}){
          status
          message
          result{
            cns{
              cns
            }
          }
        }
        addCns(input: {cpf:\"#{cpf}\",cns:\"#{cns}\"}){
          result{
            cns
          }
        }
      }
    ]
  end

  defp add_aux_emergencial_mutation(cpf, valor, nis, cidade_cod, mes, rpr_legal_cpf) do
    ~s[
      mutation{
        addAuxEmergReceiver(input: {cpf:\"#{cpf}\", valor:#{valor}, cidadeCod:\"#{cidade_cod}\", mesDisponibilizacao:\"#{
      mes
    }\", representanteLegalCpf:\"#{rpr_legal_cpf}\"}) {
          result {
            cpf
          }errors{
            explain
            field
          }
        }
        addNis(input: {cpf:\"#{cpf}\", nis:\"#{nis}\"}){
          result{
            cpf
          }errors{
            explain
            field
          }
        }
      }
    ]
  end

  defp query(data, first) do
    "{
      pfs(first: #{first}){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  defp query(data, first, cursor) do
    "{
      pfs(first: #{first}, after: \"#{cursor}\"){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  defp query_cns(data, first, cursor \\ nil)

  defp query_cns(data, first, nil) do
    "{
      cns(first: #{first}){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  defp query_cns(data, first, cursor) do
    "{
      cns(first: #{first}, after: \"#{cursor}\"){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  defp filtered_query(data, first, filter, nil) do
    filter = filter |> inspect() |> String.replace("%", "")

    "{
      pfs(first: #{first}, filter: #{filter}){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  defp filtered_query(data, first, filter, cursor) do
    filter = filter |> inspect() |> String.replace("%", "")
    "{
      pfs(first: #{first}, filter: #{filter}, after: \"#{cursor}\"){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  defp searched_cpfs(data, filter, limit \\ 1, cursor \\ nil)

  defp searched_cpfs(data, filter, limit, nil) do
    filter = filter |> inspect() |> String.replace("%", "")
    "{
      searchedCpfs(first: #{limit}, filter: #{filter}){
        edges {
          node {
             #{data}
            }
          }
          pageInfo{
            endCursor
          startCursor
          }
       }
    }"
  end

  defp searched_cpfs(data, filter, limit, cursor) do
    filter = filter |> inspect() |> String.replace("%", "")
    "{
      searchedCpfs(first:#{limit},filter:#{filter},after:\"#{cursor}\"){
        edges {
          node {
             #{data}
            }
          }
          pageInfo{
            endCursor
          startCursor
          }
       }
    }"
  end

  def query_from_elastic(data, first, nil) do
    "{
      consultaElastic(first: #{first}){
        edges{
          node{
            #{data}
          }
        },
        endCpf
        startCpf
      }
    }"
  end

  def query_from_elastic(data, first, cpf) when is_bitstring(cpf) do
    "{
      consultaElastic(first: #{first}, searchAfter: \"#{cpf}\"){
        edges{
          node{
            #{data}
          }
        },
        endCpf
        startCpf
      }
    }"
  end

  def query_from_elastic(data, first, filter) do
    filter = filter |> inspect() |> String.replace("%", "")
    "{
      consultaElastic(first: #{first}, filter: #{filter}){
        edges{
          node{
            #{data}
          }
        },
        endCpf
        startCpf
      }
    }"
  end

  def query_from_elastic(data, first, filter, cpf) do
    filter = filter |> inspect() |> String.replace("%", "")
    "{
      consultaElastic(first: #{first}, filter: #{filter}, searchAfter: \"#{cpf}\"){
        edges{
          node{
            #{data}
          }
        },
        endCpf
        startCpf
      }
    }"
  end

  def query_exec(query, span_ctx) do
    opts = [hackney: [timeout: 30_000, recv_timeout: 30_000]]
    request = HTTPoison.post(@endpoint, query, [span_ctx: span_ctx], opts)

    case request do
      {:ok, %_{body: body}} -> body |> Jason.decode!(keys: :atoms)
      other -> IO.inspect(other)
    end
  end

  def cursor_extract(stream, node) do
    stream
    |> Map.get(:data)
    |> Map.get(node)
    |> Map.get(:pageInfo)
    |> Map.get(:endCursor)
  end

  def node_extract(stream, node) do
    stream
    |> Map.get(:data)
    |> Map.get(node)
    |> Map.get(:edges)
    |> Enum.map(&Map.get(&1, :node))
  end
end
