defmodule AlertrackHelpers.Normalize do
  @moduledoc """
  Módulo auxiliar para formatação de dados de entrada
  """
  def cpf(cpf) do
    cpf(cpf, :no_signs)
    |> String.replace(~r/(\d{3})/, fn x -> x <> "." end)
    |> String.replace(~r/[.](\d{2})$/, fn x -> String.replace(x, ".", "-") end)
  end

  def cpf(cpf, :no_signs) do
    Regex.replace(~r/\D/, cpf, "")
  end

  def date(date, sign \\ "/")

  def date(date, sign) when is_list(date) do
    date
    |> Enum.filter(& &1)
    |> hd()
    |> date(sign)
  end

  def date(date, :no_signs) do
    Regex.replace(~r/\D/, date, "")
  end

  def date(date, sign) do
    handle_date(
      sign,
      date(date, :no_signs)
      |> String.codepoints()
    )
  end

  def to_short_datetype(date) when is_bitstring(date) do
    date |> Timex.parse!("{0M}/{YYYY}") |> Timex.to_date()
  end

  def to_datetype(date) when is_bitstring(date) do
    date |> Timex.parse!("{D}/{0M}/{YYYY}") |> Timex.to_date()
  end

  defp handle_date(sign, [d, d2, m, m2, y, y2]) do
    day = [d, d2] |> Enum.join()
    month = [m, m2] |> Enum.join()
    year = [y, y2] |> Enum.join()
    [day, month, year] |> Enum.join(sign)
  end

  defp handle_date(sign, [d, d2, m, m2]) do
    day = [d, d2] |> Enum.join()
    month = [m, m2] |> Enum.join()
    [day, month] |> Enum.join(sign)
  end

  defp handle_date(sign, [d, d2, m, m2, y, y2, y3, y4]) do
    day = [d, d2] |> Enum.join()
    month = [m, m2] |> Enum.join()
    year = [y, y2, y3, y4] |> Enum.join()
    [day, month, year] |> Enum.join(sign)
  end

  def cnpj(cnpj) do
    cnpj(cnpj, :no_signs)
    |> String.replace(~r/(\d{3})/, fn x -> x <> "." end)
    |> String.replace(~r/[.](\d{2})$/, fn x -> String.replace(x, ".", "-") end)
  end

  def cnpj(cnpj, :no_signs) do
    Regex.replace(~r/\D/, cnpj, "")
  end

  def clean_data_nasc(data) do
    Regex.replace(~r/\D/, data, "")
  end

  def check_formatted_cpf(_doc, ""), do: false

  def check_formatted_cpf(doc, censured_doc) do
    [_first, second | _rest] = doc |> String.split(".")
    [_f, s | _r] = censured_doc |> String.split(".")
    s == second
  end

  def proxy(proxy_string) do
    uri = URI.parse(proxy_string)
    {uri.scheme |> String.to_atom(), uri.host |> String.to_charlist(), uri.port}
  end

  def text(term) do
    term |> :binary.bin_to_list() |> List.to_string()
  end
end
