defmodule AlertrackHelpers.MyHTTP do
  def to_curl(%_{request: request}), do: curl(request)

  def curl(%HTTPoison.Request{} = req) do
    headers = req.headers |> Enum.map(fn {k, v} -> "-H \"#{k}: #{v}\"" end) |> Enum.join(" ")
    body = (req.body && req.body != "" && "-d '#{req.body}'") || nil
    params = URI.encode_query(req.params || [])
    url = [req.url, params] |> Enum.filter(&(&1 != "")) |> Enum.join("?")

    [
      "curl -v -X",
      to_string(req.method),
      headers,
      body,
      url,
      ";"
    ]
    |> Enum.filter(& &1)
    |> Enum.map(&String.trim/1)
    |> Enum.filter(&(&1 != ""))
    |> Enum.join(" ")
  end
end
