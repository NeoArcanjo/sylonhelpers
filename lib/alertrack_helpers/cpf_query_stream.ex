defmodule AlertrackHelpers.CPFQueryStream do
  @endpoint Application.get_env(:alertrack_helpers, :consulta_cpf_api)

  require OpenTelemetry.Tracer
  import AlertrackHelpers.ParseTelemetry, only: [span_ctx_encode: 0]

  def query(data, first) do
    "{
      pfs(first: #{first}){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  def query(data, first, cursor) do
    "{
      pfs(first: #{first}, after: \"#{cursor}\"){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  def filtered_query(data, first, filter) do
    filter = filter |> inspect() |> String.replace("%", "")

    "{
      pfs(first: #{first}, filter: #{filter}){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  def query_from_elastic(data, first, nil) do
    "{
      consultaElastic(first: #{first}){
        edges{
          node{
            #{data}
          }
        },
        endCpf
        startCpf
      }
    }"
  end

  def query_from_elastic(data, first, cpf) when is_bitstring(cpf) do
    "{
      consultaElastic(first: #{first}, searchAfter: \"#{cpf}\"){
        edges{
          node{
            #{data}
          }
        },
        endCpf
        startCpf
      }
    }"
  end

  def query_from_elastic(data, first, filter) do
    filter = filter |> inspect() |> String.replace("%", "")
    "{
      consultaElastic(first: #{first}, filter: #{filter}){
        edges{
          node{
            #{data}
          }
        },
        endCpf
        startCpf
      }
    }"
  end

  def query_from_elastic(data, first, filter, cpf) do
    filter = filter |> inspect() |> String.replace("%", "")
    "{
      consultaElastic(first: #{first}, filter: #{filter}, searchAfter: \"#{cpf}\"){
        edges{
          node{
            #{data}
          }
        },
        endCpf
        startCpf
      }
    }"
  end

  defp query_exec(query, span_ctx) do
    opts = [hackney: [timeout: 30_000, recv_timeout: 30_000]]

    HTTPoison.post(@endpoint, query, [span_ctx: span_ctx], opts)
    |> case do
      {:ok, %_{body: body}} -> body |> Jason.decode!(keys: :atoms)
      other -> IO.inspect(other)
    end
  end

  def filtered_query(data, first, filter, cursor) do
    filter = filter |> inspect() |> String.replace("%", "")

    "{
      pfs(first: #{first}, filter: #{filter}, after: \"#{cursor}\"){
        edges{
          node{
            #{data}
          }
        }
        pageInfo{
          endCursor
          startCursor
        }
      }
    }"
  end

  def get_cpf_birthday_by_uf(limit, uf, cursor \\ nil) do
    OpenTelemetry.Tracer.with_span "spawn_c_cpf_stream_get_cpf_birthday_by_uf" do
      span_ctx = span_ctx_encode()

      if(cursor,
        do: filtered_query("cpf birthday", limit, %{:uf => uf}, cursor),
        else: filtered_query("cpf birthday", limit, %{:uf => uf})
      )
      |> query_exec(span_ctx)
    end
  end

  def get_cpf_birthday(limit, cursor \\ nil) do
    OpenTelemetry.Tracer.with_span "spawn_c_cpf_stream_get_cpf_birthday" do
      span_ctx = span_ctx_encode()

      if(cursor,
        do: query("cpf birthday", limit, cursor),
        else: query("cpf birthday", limit)
      )
      |> query_exec(span_ctx)
    end
  end

  def get_cpf_by_uf(limit, uf, cursor \\ nil) do
    OpenTelemetry.Tracer.with_span "spawn_c_cpf_stream_get_cpf_by_uf" do
      span_ctx = span_ctx_encode()

      if(cursor,
        do: filtered_query("cpf", limit, %{:uf => uf}, cursor),
        else: filtered_query("cpf", limit, %{:uf => uf})
      )
      |> query_exec(span_ctx)
    end
  end

  def get_cpf(limit, cursor \\ nil) do
    OpenTelemetry.Tracer.with_span "spawn_c_cpf_stream_get_cpf" do
      span_ctx = span_ctx_encode()

      if(cursor, do: query("cpf", limit, cursor), else: query("cpf", limit))
      |> query_exec(span_ctx)
    end
  end

  def get_data_from_elastic(limit, filter, data \\ "cpf", search_after \\ nil) do
    OpenTelemetry.Tracer.with_span "[CPFStream] get_cpf_from_elastic/3" do
      span_ctx = span_ctx_encode()

      if(filter,
        do: query_from_elastic(data, limit, filter, search_after),
        else: query_from_elastic(data, limit, search_after)
      )
      |> query_exec(span_ctx)
    end
  end
end
