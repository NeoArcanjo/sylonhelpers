defmodule AlertrackHelpers.ResolveCaptcha do
  def detran_es(captcha, out \\ "captchas/out.png", tess_out \\ "resolve", delete_after \\ true) do
    System.cmd(
      "sh",
      [
        "./textcleaner",
        "-g",
        "-e",
        "stretch",
        "-f",
        "300",
        "-o",
        "50",
        "-t",
        "30",
        "-s",
        "1",
        "-u",
        captcha,
        out
      ],
      cd: "lib/scripts"
    )

    System.cmd("tesseract", [out, tess_out], cd: "lib/scripts")

    result =
      File.read!("lib/scripts/#{tess_out}.txt")
      |> String.trim_trailing("\n\f")

    if delete_after do
      File.rm(out)
      File.rm(tess_out)
    end
  end
end
