defmodule AlertrackHelpers.Request do
  @moduledoc """
  See `#{__MODULE__}.autoretry/2`
  """

  alias AlertrackHelpers.Request

  @max_attempts 5
  @reattempt_wait 1_000
  @doc """
  Recebe um comando de busca HTTP e tentará e tentará repetidamente até um valor máximo, sem qualquer intervenção em nome do usuário
  Example:
      HTTPoison.get("https://www.example.com")
      # Tentará novamente #{@max_attempts} vezes esperando #{@reattempt_wait / 1_000}s entre cada tentativa antes de retornar
      # Obs: Configurações abaixo são as iguais ao padrão
      |> autoretry(max_attempts: #{@max_attempts}, wait: #{@reattempt_wait}, include_404s: false, retry_unknown_errors: false)
      # Sua função que lidará com a resposta após o sucesso ou as 5 tentativas com falha
      |> handle_response()
  """
  defmacro autoretry(attempt, opts \\ []) do
    quote location: :keep, generated: true do
      attempt_fn = fn -> unquote(attempt) end

      opts =
        Keyword.merge(
          [
            max_attempts:
              Application.get_env(:httpoison, :max_attempts) || unquote(@max_attempts),
            wait: Application.get_env(:httpoison, :wait) || unquote(@reattempt_wait),
            include_404s: Application.get_env(:httpoison, :include_404s) || false,
            retry_unknown_errors: Application.get_env(:httpoison, :retry_unknown_errors) || false,
            attempt: 1
          ],
          unquote(opts)
        )

      case attempt_fn.() do
        # Error conditions
        {:error, %HTTPoison.Error{id: nil, reason: :nxdomain}} ->
          Request.next_attempt(attempt_fn, opts)

        {:error, %HTTPoison.Error{id: nil, reason: :timeout}} ->
          Request.next_attempt(attempt_fn, opts)

        {:error, %HTTPoison.Error{id: nil, reason: :closed}} ->
          Request.next_attempt(attempt_fn, opts)

        {:error, %HTTPoison.Error{id: nil, reason: _}} = response ->
          if Keyword.get(opts, :retry_unknown_errors) do
            Request.next_attempt(attempt_fn, opts)
          else
            response
          end

        # OK conditions
        {:ok, %HTTPoison.Response{status_code: 500}} ->
          Request.next_attempt(attempt_fn, opts)

        {:ok, %HTTPoison.Response{status_code: 404}} = response ->
          if Keyword.get(opts, :include_404s) do
            Request.next_attempt(attempt_fn, opts)
          else
            response
          end

        response ->
          response
      end
    end
  end

  def next_attempt(attempt, opts) do
    Process.sleep(opts[:wait])

    if opts[:max_attempts] == :infinity || opts[:attempt] < opts[:max_attempts] - 1 do
      opts = Keyword.put(opts, :attempt, opts[:attempt] + 1)
      autoretry(attempt.(), opts)
    else
      attempt.()
    end
  end
end
