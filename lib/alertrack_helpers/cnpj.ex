defmodule AlertrackHelpers.Cnpj do
  @moduledoc """

  """
  alias AlertrackHelpers.Normalize

  @cnpj_file "priv/cnpjs/cnpj_"
  @digit13 (5..2 |> Enum.to_list()) ++ (9..2 |> Enum.to_list())
  @digit14 (6..2 |> Enum.to_list()) ++ (9..2 |> Enum.to_list())
  @timeout Application.get_env(:fidelity, :timeout)
  @recv_timeout Application.get_env(:fidelity, :recv_timeout)

  def complete(cnpj), do: complete(cnpj, formatted: false)

  def complete(cnpj, formatted: option) do
    cnpj =
      Normalize.cnpj(cnpj, :no_signs)
      |> String.slice(0..8)
      |> String.split("")
      |> Enum.filter(fn x -> x != "" end)
      |> Enum.map(&String.to_integer/1)

    digit13 = 0 |> calc(cnpj, @digit13) |> Kernel.*(10) |> rem(11) |> rem(10)
    digit14 = 0 |> calc(cnpj ++ [digit13], @digit14) |> Kernel.*(10) |> rem(11) |> rem(10)
    (cnpj ++ [digit13, digit14]) |> Enum.join() |> format_if_necessary(option)
  end

  defp format_if_necessary(cnpj, true), do: Normalize.cnpj(cnpj)
  defp format_if_necessary(cnpj, false), do: Normalize.cnpj(cnpj, :no_signs)

  def calc(total, [], []), do: total

  def calc(total, [num1 | rest1], [num2 | rest2]), do: calc(total + num1 * num2, rest1, rest2)

  @doc """
  Dado o nome de uma empresa, retorna, se encontrado(s) CNPJ(s) de pessoas possivelmente cadastradas
  """
  def get_from(term) do
    ~s(https://www.google.com/search?q=site%3Areclameaqui.com.br+inurl%3A%2F#{term}*%2F+"cnpj")
    |> HTTPoison.get!([], timeout: @timeout, recv_timeout: @recv_timeout)
    |> body_response()
    |> find_cnpjs()
  end

  defp body_response(%_{body: body}), do: body

  defp find_cnpjs(html) do
    Regex.scan(~r/\s\d{3}\.?\d{3}\.?\d{3}\.?\-?\s?\d{2}/, html, capture: :first)
    |> List.flatten()
    |> Enum.map(&complete/1)
  end

  def generate do
    File.mkdir("priv/cnpjs/")

    0..999_999_999_999
    |> Stream.take(100)
    |> Task.async_stream(
      fn cnpj -> create(cnpj) end,
      max_cuncurrency: 1000
    )
    |> Stream.run()
  end

  defp create(cnpj) do
    copy = cnpj |> div(10_000)

    cnpj =
      cnpj
      |> Integer.to_string()
      |> String.pad_leading(12, ["0"])
      |> complete()
      |> add_line()
      |> (fn x -> File.write("#{@cnpj_file}#{inspect(copy)}.txt", x, [:append]) end).()
  end

  def add_line(cnpj), do: cnpj <> "\n"

  def cnpj_validate(cnpj) do
    cnpj = Normalize.cnpj(cnpj, :no_signs)
    cnpj_series = cnpj |> String.split("") |> Enum.filter(fn x -> x != "" end)
    first = String.slice(cnpj, 0..0)

    cond do
      Enum.all?(cnpj_series, fn x -> x == first end) -> false
      cnpj != complete(cnpj) -> false
      true -> true
    end
  end
end
