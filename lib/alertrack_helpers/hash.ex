defmodule AlertrackHelpers.Hash do
  @moduledoc false
  @spec random_string(non_neg_integer) :: binary
  def random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64() |> binary_part(0, length)
  end
end
