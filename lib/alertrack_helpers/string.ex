defmodule AlertrackHelpers.String do
  def insert_at(string, pos, term) do
    tam = String.length(string)
    start = String.slice(string, 0..(pos - 1))
    final = String.slice(string, pos..tam)
    start <> term <> final
  end
end
