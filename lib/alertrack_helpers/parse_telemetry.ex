defmodule AlertrackHelpers.ParseTelemetry do
  def first_or_nil([]), do: nil

  def first_or_nil(list) when is_list(list), do: hd(list)

  def first_or_nil(_other), do: nil

  def get_parent_span(nil), do: nil

  def get_parent_span({"span_ctx", value}) do
    value
    |> Jason.decode!()
    |> Enum.map(fn
      x when is_bitstring(x) -> String.to_atom(x)
      x -> x
    end)
    |> List.to_tuple()
  end

  def span_ctx_encode do
    require OpenTelemetry.Tracer

    OpenTelemetry.Tracer.current_span_ctx()
    |> Tuple.to_list()
    |> Jason.encode!()
  end
end
