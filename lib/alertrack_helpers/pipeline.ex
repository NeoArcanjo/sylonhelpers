defmodule AlertrackHelpers.Pipeline do
  @time_factor 1_000
  @endpoint Application.get_env(:alertrack_helpers, :consulta_cpf_api)
  @timeout Application.get_env(:httpoison, :timeout)
  @recv_timeout Application.get_env(:httpoison, :recv_timeout)
  @elastic_url Application.get_env(:elastix, :elastic_url)

  require Logger
  alias AlertrackHelpers.Fetcher
  alias AlertrackHelpers.Normalize
  require OpenTelemetry.{Observer, Span, Tracer}
  import AlertrackHelpers.Json, only: [body_decode: 1]

  # configurações pré-definidas de timeout para as requisições
  def help_telemetry do
    quote do
      require OpenTelemetry.{Observer, Span, Tracer}
      import AlertrackHelpers.ParseTelemetry, only: [span_ctx_encode: 0]
    end
  end

  # def help_aliases do
  # quote do

  # end
  # end

  # def help_imports do
  # quote do

  # end
  # end

  def help_requires do
    quote do
      require Logger
    end
  end

  @doc """
  When used, dispatch to the appropriate helpers.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end

  def calcule_stream(tag, limit, opts \\ [])

  def calcule_stream(:notifica, limit, opts) do
    opts =
      opts
      |> Keyword.put(:limit, limit)

    Fetcher.get_pfs("cpf", opts)
  end

  def calcule_stream(:credilink, limit, opts) do
    opts =
      opts
      |> Keyword.put(:limit, limit)
      |> Keyword.put(:filter, %{has_birthday: false})

    Fetcher.get_pfs("cpf", opts)
  end

  def calcule_stream(:cadsus, limit, opts) do
    opts =
      opts
      |> Keyword.put(:limit, limit)
      |> Keyword.put(:filter, %{has_birthday: false})

    Fetcher.get_pfs("cpf", opts)
  end

  def calcule_stream(:cadsus_cns, limit, opts) do
    opts = Keyword.put(opts, :limit, limit)
    Fetcher.get_cns("cpf cns", opts)
  end

  def calcule_stream(:cns, limit, opts) do
    opts =
      opts
      |> Keyword.put(:limit, limit)
      |> Keyword.put(:filter, %{has_birthday: true, cns_nil: true})

    Fetcher.get_pfs("cpf birthday", opts)
  end

  # def calcule_stream(:get, limit, opts) do
  #   cursor = opts[:cursor]

  #   Fetcher.get_pfs("cpf", limit, %{has_birthday: false, has_name: true}, cursor)
  # end

  def calcule_stream(:cartao_sus, limit, opts) do
    opts = opts |> Keyword.put(:limit, limit)

    Fetcher.get_pfs("cpf", opts)
    Fetcher.get_data_from_elastic("cpf birthday emails", opts)
  end

  def calcule_stream(:gov, limit, opts) do
    opts =
      opts
      |> Keyword.put(:limit, limit)

    Fetcher.get_pfs("cpf", opts)
  end

  def time_wait(opts) do
    tag = Keyword.get(opts, :tag, "test")
    OpenTelemetry.Tracer.start_span("#{tag}_time_wait")

    now = Time.utc_now()

    out =
      case Time.diff(now, ~T[21:00:00.000000]) do
        x when x in [0..10799] -> 200
        x when x in [-75600..-54001] -> 200
        _ -> 2_000
      end

    OpenTelemetry.Tracer.end_span()
    out
  end

  def look_index(search) do
    case search do
      {:ok, %_{body: %{hits: %{hits: []}}}} ->
        nil

      {:ok, %_{body: %{hits: %{hits: [matchs]}}}} ->
        matchs
        |> Map.get(:_source)
        |> Map.get(:last_entry)
    end
  end

  def get_savepoint(initial, process_name) when is_map(initial) do
    if initial[:cursor] do
      initial
    else
      cursor =
        Elastix.Search.search(
          @elastic_url,
          "process_checkpoint",
          [],
          %{
            query: %{
              term: %{process_name: process_name}
            }
          }
        )
        |> look_index()

      %{initial | cursor: cursor}
    end
  end

  def get_savepoint(initial, process_name) do
    if initial[:cursor] do
      initial
    else
      cursor =
        Elastix.Search.search(
          @elastic_url,
          "process_checkpoint",
          [],
          %{
            query: %{
              term: %{process_name: process_name}
            }
          }
        )
        |> look_index()

      Keyword.put(initial, :cursor, cursor)
    end
  end

  def search_in_index(cpf, tag) do
    OpenTelemetry.Tracer.start_span("#{tag}_look_for_document_in_index")
    OpenTelemetry.Span.set_attributes(cpf: cpf)

    out =
      Elastix.Search.search(
        @elastic_url,
        "#{tag}_log*",
        [],
        %{
          query: %{
            bool: %{
              must_not: [%{match: %{return: false}}],
              filter: %{term: %{cpf: cpf}}
            }
          }
        }
      )

    OpenTelemetry.Tracer.end_span()
    out
  end

  def has_name?(cpf) do
    Fetcher.get_cpf_combination("name", %{cpf: cpf, has_name: true})
    |> Fetcher.node_extract(:searchedCpfs)
    |> List.first()
    |> name_check()
  end

  defp name_check(nil), do: false
  defp name_check(%{name: nil}), do: false
  defp name_check(_), do: true

  def pf_exists?(cpf) do
    Fetcher.get_cpf_combination("cpf", %{cpf: cpf})
    |> Fetcher.node_extract(:searchedCpfs)
    |> List.first()
    |> (&(not is_nil(&1))).()
  end

  def get_birthday(nil), do: []
  def get_birthday(:not_found), do: []

  def get_birthday(map) when is_map(map) do
    OpenTelemetry.Tracer.start_span("cadsus_get_birthday")
    direct = Map.get(map, :dataNascimento, Map.get(map, :data_nascimento))

    inner = get_in(map, [:usuario, [:dataNascimento]])

    next_inner = get_in(map, [:usuario, [:data_nascimento]])

    out =
      [direct, inner, next_inner]
      |> Enum.uniq()
      |> Enum.filter(&is_binary(&1))
      |> handle_birthday()
      |> List.first()

    OpenTelemetry.Tracer.end_span()
    out
  end

  @spec handle_birthday(maybe_improper_list) :: [binary]
  def handle_birthday(lista) when is_list(lista) do
    OpenTelemetry.Tracer.start_span("cadsus_handle_birthday")

    out =
      lista
      |> Enum.reject(&is_nil(&1))
      |> Enum.map(&Normalize.date(&1, "/"))
      |> Enum.map(fn x ->
        (String.length(x) != 10 && String.replace(x, ~r[/(?=\d{2}$)], "/19")) || x
      end)

    OpenTelemetry.Tracer.end_span()
    out
  end
end
