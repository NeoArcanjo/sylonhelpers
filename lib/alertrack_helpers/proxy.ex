defmodule AlertrackHelpers.Proxy do
  @moduledoc """
  85.90.215.111	3128
  85.147.153.34   80
  185.72.27.10	8080
  185.72.27.12	8080
  81.201.60.130	80
  79.104.25.218	8080

  https://www.proxynova.com/proxy-server-list/elite-proxies/
  """
end
