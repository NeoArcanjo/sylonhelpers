defmodule AlertrackHelpers.Json do
  import AlertrackHelpers.TypeOf, only: [type_of: 1]

  def body_decode(body) do
    body
    |> Jason.decode!(keys: :atoms)
  rescue
    _e -> type_of(body)
  end
end
