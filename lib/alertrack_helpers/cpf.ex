defmodule AlertrackHelpers.Cpf do
  @moduledoc """

  """
  alias AlertrackHelpers.Normalize

  @digit10 10..2 |> Enum.to_list()
  @digit11 11..2 |> Enum.to_list()
  @timeout 30_000
  @recv_timeout 30_000

  def complete(cpf), do: complete(cpf, formatted: false)

  def complete(cpf, formatted: option) do
    cpf =
      Normalize.cpf(cpf, :no_signs)
      |> String.slice(0..8)
      |> String.split("")
      |> Enum.filter(fn x -> x != "" end)
      |> Enum.map(&String.to_integer/1)

    digit10 = 0 |> calc(cpf, @digit10) |> Kernel.*(10) |> rem(11) |> rem(10)
    digit11 = 0 |> calc(cpf ++ [digit10], @digit11) |> Kernel.*(10) |> rem(11) |> rem(10)
    (cpf ++ [digit10, digit11]) |> Enum.join() |> format_if_necessary(option)
  end

  @doc """
  Dado o nome de uma empresa, retorna, se encontrado(s) CPF(s) de pessoas possivelmente cadastradas
  """
  def get_from(term) do
    ~s(https://www.google.com/search?q=site%3Areclameaqui.com.br+inurl%3A%2F#{term}*%2F+"cpf")
    |> HTTPoison.get!([], timeout: @timeout, recv_timeout: @recv_timeout)
    |> body_response()
    |> find_cpfs()
  end

  defp body_response(%_{body: body}), do: body

  defp find_cpfs(html) do
    Regex.scan(~r/\s\d{3}\.?\d{3}\.?\d{3}\.?\-?\s?\d{2}/, html, capture: :first)
    |> List.flatten()
    |> Enum.map(&complete/1)
  end

  defp format_if_necessary(cpf, true), do: Normalize.cpf(cpf)
  defp format_if_necessary(cpf, false), do: Normalize.cpf(cpf, :no_signs)

  def calc(total, [], []), do: total

  def calc(total, [num1 | rest1], [num2 | rest2]), do: calc(total + num1 * num2, rest1, rest2)

  def validate(cpf) do
    cpf = Normalize.cpf(cpf, :no_signs)
    cpf_series = cpf |> String.split("") |> Enum.filter(fn x -> x != "" end)
    first = String.slice(cpf, 0..0)

    cond do
      Enum.all?(cpf_series, fn x -> x == first end) -> false
      cpf != complete(cpf) -> false
      true -> true
    end
  end

  @doc """
  Dado o nome de uma empresa, retorna, se encontrado(s) CPF(s) de pessoas possivelmente cadastradas
  """
  def get_from(term) do
    ~s(https://www.google.com/search?q=site%3Areclameaqui.com.br+inurl%3A%2F#{term}*%2F+"cpf")
    |> HTTPoison.get!([], timeout: @timeout, recv_timeout: @recv_timeout)
    |> body_response()
    |> find_cpfs()
  end

  defp body_response(%_{body: body}), do: body

  defp find_cpfs(html) do
    Regex.scan(~r/\s\d{3}\.?\d{3}\.?\d{3}\.?\-?\s?\d{2}/, html, capture: :first)
    |> List.flatten()
    |> Enum.map(&complete/1)
  end
end
