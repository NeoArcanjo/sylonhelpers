defmodule AlertrackHelpers.Index do
  @elastic_url Application.get_env(:elastix, :elastic_url)

  def create_if_not_exists(index) do
    if {:ok, false} == Elastix.Index.exists?(@elastic_url, index) do
      Elastix.Index.create(@elastic_url, index, %{})
    end
  end

  def delete_if_exists(index) do
    unless {:ok, false} == Elastix.Index.exists?(@elastic_url, index) do
      Elastix.Index.delete(@elastic_url, index)
    end
  end
end
