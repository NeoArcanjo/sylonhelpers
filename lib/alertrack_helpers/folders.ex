defmodule AlertrackHelpers.Folder do
  def create_if_not_exists(folder) do
    unless File.exists?(folder) and File.dir?(folder) do
      File.mkdir_p(folder)
    end
  end
end
